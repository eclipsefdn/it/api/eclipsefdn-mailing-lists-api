/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.mailing.resources;

import org.eclipsefoundation.mailing.test.namespace.SchemaNamespaceHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import io.quarkus.test.junit.QuarkusTest;

@TestInstance(Lifecycle.PER_CLASS)
@QuarkusTest
class MailingListsResourceTests {

    public static final String MAILING_LISTS_BASE_URL = "mailing-list";
    public static final String MAILING_LISTS_FOR_USER = MAILING_LISTS_BASE_URL + "?username={param}";
    public static final String AVAILABLE_MAILING_LISTS_URL = MAILING_LISTS_BASE_URL + "/available";
    public static final String MAILING_LISTS_BY_NAME_URL = MAILING_LISTS_BASE_URL + "/{listName}";

    public static final String PROJECTS_MAILING_LISTS_URL = MAILING_LISTS_BASE_URL + "/projects";
    public static final String AVAILABLE_PROJECTS_MAILING_LISTS_URL = PROJECTS_MAILING_LISTS_URL + "/available";
    public static final String PROJECT_MAILING_LISTS_URL = PROJECTS_MAILING_LISTS_URL + "/{projectID}";

    /*
     * GET ALL
     */
    public static final EndpointTestCase GET_ALL_SUCCESS = TestCaseHelper
            .buildSuccessCase(MAILING_LISTS_BASE_URL, new String[] {}, SchemaNamespaceHelper.MAILING_LISTS_SCHEMA_PATH);

    /*
     * GET ALL FOR USER
     */
    public static final EndpointTestCase GET_ALL_FOR_USER_SUCCESS = TestCaseHelper
            .buildSuccessCase(MAILING_LISTS_FOR_USER, new String[] { "testytesterson" },
                    SchemaNamespaceHelper.MAILING_LISTS_SCHEMA_PATH);

    /*
     * GET AVAILABLE
     */
    public static final EndpointTestCase GET_AVAILABLE_SUCCESS = TestCaseHelper
            .buildSuccessCase(AVAILABLE_MAILING_LISTS_URL, new String[] {},
                    SchemaNamespaceHelper.MAILING_LISTS_SCHEMA_PATH);

    /*
     * BY NAME
     */
    public static final EndpointTestCase GET_MAILING_LISTS_BY_NAME_SUCCESS = TestCaseHelper
            .buildSuccessCase(MAILING_LISTS_BY_NAME_URL, new String[] { "eclipse-dev" },
                    SchemaNamespaceHelper.MAILING_LISTS_SCHEMA_PATH);

    public static final EndpointTestCase GET_MAILING_LISTS_BY_NAME_NOT_FOUND = TestCaseHelper
            .buildNotFoundCase(MAILING_LISTS_BY_NAME_URL, new String[] { "elipse-dem" }, null);

    /*
     * GET PROJECTS MAILING LISTS
     */
    public static final EndpointTestCase GET_PROJECTS_MAILING_LISTS_SUCCESS = TestCaseHelper
            .buildSuccessCase(PROJECTS_MAILING_LISTS_URL, new String[] {},
                    SchemaNamespaceHelper.MAILING_LIST_MAPPING_SCHEMA_PATH);

    /*
     * GET AVAILABLE PROJECTS
     */
    public static final EndpointTestCase GET_AVAILABLE_PROJECTS_MAILING_LISTS_SUCCESS = TestCaseHelper
            .buildSuccessCase(AVAILABLE_PROJECTS_MAILING_LISTS_URL, new String[] {},
                    SchemaNamespaceHelper.MAILING_LIST_MAPPING_SCHEMA_PATH);

    /*
     * PROJECTS MAILING LISTS BY NAME
     */
    public static final EndpointTestCase GET_PROJECTS_MAILING_LISTS_BY_ID_SUCCESS = TestCaseHelper
            .buildSuccessCase(PROJECT_MAILING_LISTS_URL, new String[] { "ee4j" },
                    SchemaNamespaceHelper.MAILING_LISTS_SCHEMA_PATH);

    /*
     * GET ALL
     */
    @Test
    void getMailingLists_success() {
        EndpointTestBuilder.from(GET_ALL_SUCCESS).run();
    }

    @Test
    void getMailingLists_success_validResponseFormat() {
        EndpointTestBuilder.from(GET_ALL_SUCCESS).andCheckFormat().run();
    }

    @Test
    void getMailingLists_success_validSchema() {
        EndpointTestBuilder.from(GET_ALL_SUCCESS).andCheckSchema().run();
    }

    /*
     * GET ALL FOR USER
     */
    @Test
    void getMailingListsForUser_success() {
        EndpointTestBuilder.from(GET_ALL_FOR_USER_SUCCESS).run();
    }

    @Test
    void getMailingListsForUser_success_validResponseFormat() {
        EndpointTestBuilder.from(GET_ALL_FOR_USER_SUCCESS).andCheckFormat().run();
    }

    @Test
    void getMailingListsForUser_success_validSchema() {
        EndpointTestBuilder.from(GET_ALL_FOR_USER_SUCCESS).andCheckSchema().run();
    }

    @Test
    void getMailingListsForUser_failure_notFound() {
        EndpointTestBuilder
                .from(TestCaseHelper.buildNotFoundCase(MAILING_LISTS_FOR_USER, new String[] { "nope" }, null)).run();
    }

    /*
     * GET AVAILABLE
     */
    @Test
    void getAvailableMailingLists_success() {
        EndpointTestBuilder.from(GET_AVAILABLE_SUCCESS).run();
    }

    @Test
    void getAvailableMailingLists_success_validResponseFormat() {
        EndpointTestBuilder.from(GET_AVAILABLE_SUCCESS).andCheckFormat().run();
    }

    @Test
    void getAvailableMailingLists_success_validSchema() {
        EndpointTestBuilder.from(GET_AVAILABLE_SUCCESS).andCheckSchema().run();
    }

    /*
     * BY NAME
     */
    @Test
    void getMailingList_success() {
        EndpointTestBuilder.from(GET_MAILING_LISTS_BY_NAME_SUCCESS).run();
    }

    @Test
    void getMailingList_success_validResponseFormat() {
        EndpointTestBuilder.from(GET_MAILING_LISTS_BY_NAME_SUCCESS).andCheckFormat().run();
    }

    @Test
    void getMailingList_success_validSchema() {
        EndpointTestBuilder.from(GET_MAILING_LISTS_BY_NAME_SUCCESS).andCheckSchema().run();
    }

    void getMailingList_failure_notFound() {
        EndpointTestBuilder.from(GET_MAILING_LISTS_BY_NAME_NOT_FOUND).run();
    }

    /*
     * GET PROJECTS MAILING LISTS
     */
    @Test
    void getProjectsMailingLists_success() {
        EndpointTestBuilder.from(GET_PROJECTS_MAILING_LISTS_SUCCESS).run();
    }

    @Test
    void getProjectsMailingLists_success_validResponseFormat() {
        EndpointTestBuilder.from(GET_PROJECTS_MAILING_LISTS_SUCCESS).andCheckFormat().run();
    }

    @Test
    void getProjectsMailingLists_success_validSchema() {
        EndpointTestBuilder.from(GET_PROJECTS_MAILING_LISTS_SUCCESS).andCheckSchema().run();
    }

    /*
     * GET AVILABLE PROJECTS MAILING LISTS
     */
    @Test
    void getAvailableProjectsMailingLists_success() {
        EndpointTestBuilder.from(GET_AVAILABLE_PROJECTS_MAILING_LISTS_SUCCESS).run();
    }

    @Test
    void getAvailableProjectsMailingLists_success_validResponseFormat() {
        EndpointTestBuilder.from(GET_AVAILABLE_PROJECTS_MAILING_LISTS_SUCCESS).andCheckFormat().run();
    }

    @Test
    void getAvailableProjectsMailingLists_success_validSchema() {
        EndpointTestBuilder.from(GET_AVAILABLE_PROJECTS_MAILING_LISTS_SUCCESS).andCheckSchema().run();
    }

    /*
     * GET PROJECT MAILING LIST BY ID
     */
    @Test
    void getProjectMailingList_success() {
        EndpointTestBuilder.from(GET_PROJECTS_MAILING_LISTS_BY_ID_SUCCESS).run();
    }

    @Test
    void getProjectMailingList_success_validResponseFormat() {
        EndpointTestBuilder.from(GET_PROJECTS_MAILING_LISTS_BY_ID_SUCCESS).andCheckFormat().run();
    }

    @Test
    void getProjectMailingList_success_validSchema() {
        EndpointTestBuilder.from(GET_PROJECTS_MAILING_LISTS_BY_ID_SUCCESS).andCheckSchema().run();
    }
}