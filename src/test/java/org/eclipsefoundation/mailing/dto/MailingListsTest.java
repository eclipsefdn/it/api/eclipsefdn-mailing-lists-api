/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.mailing.dto;

import java.net.URI;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.eclipsefoundation.http.model.FlatRequestWrapper;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.mailing.namespace.MailingListUrlParameterNames;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;

@QuarkusTest
class MailingListsTest {

    private static final RequestWrapper wrap = new FlatRequestWrapper(URI.create("api.eclipse.org"));

    @Inject
    DefaultHibernateDao dao;
    @Inject
    FilterService filters;

    @Test
    void getMailingListByName() {
        MailingLists created = generateAndPersistMailingLists("llm4j");
        Assertions.assertNotNull(created);

        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(MailingListUrlParameterNames.LIST_NAME.getName(), created.getListName());
        MailingLists searchResult = dao.get(new RDBMSQuery<>(wrap, filters.get(MailingLists.class), params)).get(0);

        Assertions.assertEquals(created, searchResult);

        // Search for invalid list
        params = new MultivaluedHashMap<>();
        params.add(MailingListUrlParameterNames.LIST_NAME.getName(), "nope");
        List<MailingLists> results = dao.get(new RDBMSQuery<>(wrap, filters.get(MailingLists.class), params));
        Assertions.assertTrue(results.isEmpty());
    }

    @Test
    void getMailingListByProjectId() {
        MailingLists created = generateAndPersistMailingLists("test4j");
        Assertions.assertNotNull(created);

        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(MailingListUrlParameterNames.PROJECT_ID.getName(), created.getProjectId());
        MailingLists searchResult = dao.get(new RDBMSQuery<>(wrap, filters.get(MailingLists.class), params)).get(0);

        Assertions.assertEquals(created, searchResult);

        // Search for invalid list
        params = new MultivaluedHashMap<>();
        params.add(MailingListUrlParameterNames.PROJECT_ID.getName(), "nope");
        List<MailingLists> results = dao.get(new RDBMSQuery<>(wrap, filters.get(MailingLists.class), params));
        Assertions.assertTrue(results.isEmpty());
    }

    @Test
    void getMailingListBySubscribbable() {
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(MailingListUrlParameterNames.IS_SUBSCRIBABLE.getName(), Boolean.TRUE.toString());
        List<MailingLists> searchResults = dao.get(new RDBMSQuery<>(wrap, filters.get(MailingLists.class), params));

        Assertions.assertFalse(searchResults.isEmpty());
        Assertions.assertEquals(4, searchResults.size());

        // Validate the inverse
        params = new MultivaluedHashMap<>();
        params.add(MailingListUrlParameterNames.IS_SUBSCRIBABLE.getName(), Boolean.FALSE.toString());
        searchResults = dao.get(new RDBMSQuery<>(wrap, filters.get(MailingLists.class), params));

        Assertions.assertTrue(searchResults.isEmpty());
    }

    @Test
    void getMailingListByIsPrivate() {
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(MailingListUrlParameterNames.IS_PRIVATE.getName(), Boolean.TRUE.toString());
        List<MailingLists> searchResults = dao.get(new RDBMSQuery<>(wrap, filters.get(MailingLists.class), params));

        Assertions.assertFalse(searchResults.isEmpty());
        Assertions.assertEquals(1, searchResults.size());

        // Validate the inverse
        params = new MultivaluedHashMap<>();
        params.add(MailingListUrlParameterNames.IS_PRIVATE.getName(), Boolean.FALSE.toString());
        searchResults = dao.get(new RDBMSQuery<>(wrap, filters.get(MailingLists.class), params));

        Assertions.assertFalse(searchResults.isEmpty());
        Assertions.assertEquals(4, searchResults.size());
    }

    @Test
    void getMailingListByIsDeleted() {
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(MailingListUrlParameterNames.IS_DELETED.getName(), Boolean.TRUE.toString());
        List<MailingLists> searchResults = dao.get(new RDBMSQuery<>(wrap, filters.get(MailingLists.class), params));

        Assertions.assertFalse(searchResults.isEmpty());
        Assertions.assertEquals(1, searchResults.size());

        // Validate the inverse
        params = new MultivaluedHashMap<>();
        params.add(MailingListUrlParameterNames.IS_DELETED.getName(), Boolean.FALSE.toString());
        searchResults = dao.get(new RDBMSQuery<>(wrap, filters.get(MailingLists.class), params));

        Assertions.assertFalse(searchResults.isEmpty());
        Assertions.assertEquals(3, searchResults.size());
    }

    @Test
    void getMailingListByIsDisabled() {
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(MailingListUrlParameterNames.IS_DISABLED.getName(), Boolean.TRUE.toString());
        List<MailingLists> searchResults = dao.get(new RDBMSQuery<>(wrap, filters.get(MailingLists.class), params));

        Assertions.assertFalse(searchResults.isEmpty());
        Assertions.assertEquals(1, searchResults.size());

        // Validate the inverse
        params = new MultivaluedHashMap<>();
        params.add(MailingListUrlParameterNames.IS_DISABLED.getName(), Boolean.FALSE.toString());
        searchResults = dao.get(new RDBMSQuery<>(wrap, filters.get(MailingLists.class), params));

        Assertions.assertFalse(searchResults.isEmpty());
        Assertions.assertEquals(2, searchResults.size());
    }

    @Test
    void getMailingListByEmail() {
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(MailingListUrlParameterNames.USER_EMAIL.getName(), "sample@eclipse.org");
        List<MailingLists> searchResults = dao.get(new RDBMSQuery<>(wrap, filters.get(MailingLists.class), params));

        Assertions.assertFalse(searchResults.isEmpty());
        Assertions.assertEquals(2, searchResults.size());

        // Invalid email
        params = new MultivaluedHashMap<>();
        params.add(MailingListUrlParameterNames.USER_EMAIL.getName(), "nope@eclipse.org");
        searchResults = dao.get(new RDBMSQuery<>(wrap, filters.get(MailingLists.class), params));

        Assertions.assertTrue(searchResults.isEmpty());
    }

    private MailingLists generateAndPersistMailingLists(String projectId) {
        MailingLists ml = new MailingLists();
        ml.setListName(projectId + "-dev");
        ml.setProjectId(projectId);
        ml.setListDescription("A really good list");
        ml.setListShortDescription("Is good");
        ml.setCreateArchives(true);
        ml.setForNewsArchives(false);
        ml.setPrivate(false);
        ml.setSubscribable(true);
        ml.setDisabled(false);
        ml.setDeleted(false);
        ml.setCreateDate(new Date());
        ml.setCreatedBy("EF staff");
        ml.setProvisionStatus("Active");
        ml.loadAdditionalProperties();
        return dao.add(new RDBMSQuery<>(wrap, filters.get(MailingLists.class)), Arrays.asList(ml)).get(0);
    }
}
