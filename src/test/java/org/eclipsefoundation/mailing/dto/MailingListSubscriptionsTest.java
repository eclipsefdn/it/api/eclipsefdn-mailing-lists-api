/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.mailing.dto;

import java.net.URI;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.eclipsefoundation.http.model.FlatRequestWrapper;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.http.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.mailing.namespace.MailingListUrlParameterNames;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;

@QuarkusTest
class MailingListSubscriptionsTest {

    private static final RequestWrapper wrap = new FlatRequestWrapper(URI.create("api.eclipse.org"));

    @Inject
    DefaultHibernateDao dao;
    @Inject
    FilterService filters;

    @Test
    void getBySubscriptionsByEmail() {
        MailingListSubscriptions created = generateAndPersistSubscription("some@mail.com", "ee4j-dev");
        Assertions.assertNotNull(created);

        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(DefaultUrlParameterNames.ID.getName(), created.getEmail());
        MailingListSubscriptions searchResult = dao
                .get(new RDBMSQuery<>(wrap, filters.get(MailingListSubscriptions.class), params)).get(0);

        Assertions.assertEquals(created, searchResult);

        // Invalid email
        params = new MultivaluedHashMap<>();
        params.add(DefaultUrlParameterNames.ID.getName(), "nope@email.com");
        List<MailingListSubscriptions> results = dao
                .get(new RDBMSQuery<>(wrap, filters.get(MailingListSubscriptions.class), params));
        Assertions.assertTrue(results.isEmpty());

        // Email with 2 subs
        params = new MultivaluedHashMap<>();
        params.add(DefaultUrlParameterNames.ID.getName(), "sample@eclipse.org");
        results = dao.get(new RDBMSQuery<>(wrap, filters.get(MailingListSubscriptions.class), params));
        Assertions.assertEquals(2, results.size());
    }

    @Test
    void getBySubscriptionsByListName() {
        MailingListSubscriptions created = generateAndPersistSubscription("someother@mail.com", "new-list");
        Assertions.assertNotNull(created);

        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(MailingListUrlParameterNames.LIST_NAME.getName(), created.getListName());
        MailingListSubscriptions searchResult = dao
                .get(new RDBMSQuery<>(wrap, filters.get(MailingListSubscriptions.class), params)).get(0);

        Assertions.assertEquals(created, searchResult);

        // Invalid list name
        params = new MultivaluedHashMap<>();
        params.add(MailingListUrlParameterNames.LIST_NAME.getName(), "nope-list");
        List<MailingListSubscriptions> results = dao
                .get(new RDBMSQuery<>(wrap, filters.get(MailingListSubscriptions.class), params));
        Assertions.assertTrue(results.isEmpty());

        // list with 2 subs
        params = new MultivaluedHashMap<>();
        params.add(MailingListUrlParameterNames.LIST_NAME.getName(), "internal-dev");
        results = dao.get(new RDBMSQuery<>(wrap, filters.get(MailingListSubscriptions.class), params));
        Assertions.assertEquals(2, results.size());
    }

    private MailingListSubscriptions generateAndPersistSubscription(String email, String listName) {
        MailingListSubscriptions sub = new MailingListSubscriptions();
        sub.setEmail(email);
        sub.setListName(listName);
        sub.setDigest(false);
        sub.setSubscribed(new Date());
        return dao.add(new RDBMSQuery<>(wrap, filters.get(MailingListSubscriptions.class)), Arrays.asList(sub)).get(0);
    }
}
