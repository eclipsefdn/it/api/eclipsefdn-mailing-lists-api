/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.mailing.test.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.efservices.api.ProfileAPI;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.api.models.EfUserBuilder;
import org.eclipsefoundation.efservices.api.models.EfUserCountryBuilder;
import org.eclipsefoundation.efservices.api.models.UserSearchParams;

import io.quarkus.test.Mock;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.NotFoundException;

@Mock
@RestClient
@ApplicationScoped
public class MockProfileAPI implements ProfileAPI {

    private List<EfUser> users;

    public MockProfileAPI() {
        this.users = new ArrayList<>();
        this.users
                .addAll(Arrays
                        .asList(EfUserBuilder
                                .builder()
                                .uid("42")
                                .name("testytesterson")
                                .fullName("Testy Testerson")
                                .githubHandle("handle")
                                .mail("testy@test.com")
                                .picture("pic url")
                                .firstName("Testy")
                                .lastName("Testerson")
                                .publisherAgreements(new HashMap<>())
                                .twitterHandle("")
                                .org("")
                                .jobTitle("employee")
                                .website("site url")
                                .country(EfUserCountryBuilder.builder().build())
                                .interests(Arrays.asList())
                                .build()));
    }

    @Override
    public List<EfUser> getUsers(String token, UserSearchParams params) {
        if (params.uid == null && StringUtils.isBlank(params.mail) && StringUtils.isBlank(params.name)) {
            return Collections.emptyList();
        }

        List<EfUser> results = Collections.emptyList();

        // Only filter via additional fields if it can't find with previous ones
        if (params.uid != null) {
            results = users.stream().filter(u -> u.uid().compareTo(params.uid) == 0)
                    .toList();
        }
        if (StringUtils.isNotBlank(params.name) && results.isEmpty()) {
            results = users.stream().filter(u -> u.name().equalsIgnoreCase(params.name))
                    .toList();
        }
        if (StringUtils.isNotBlank(params.mail) && results.isEmpty()) {
            results = users.stream().filter(u -> u.mail().equalsIgnoreCase(params.mail))
                    .toList();
        }

        return results;
    }

    @Override
    public EfUser getUserByEfUsername(String token, String username) {
        return users
                .stream()
                .filter(u -> u.name().equalsIgnoreCase(username))
                .findFirst()
                .orElseThrow(() -> new NotFoundException(String.format("User '%s' not found", username)));
    }

    @Override
    public EfUser getUserByGithubHandle(String token, String handle) {
        return users
                .stream()
                .filter(u -> u.githubHandle().equalsIgnoreCase(handle))
                .findFirst()
                .orElseThrow(() -> new NotFoundException(String.format("User '%s' not found", handle)));
    }
}
