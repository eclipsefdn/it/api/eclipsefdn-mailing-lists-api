/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.mailing.dto;

import java.util.Date;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.http.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.mailing.namespace.MailingListUrlParameterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.annotation.Nonnull;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.ws.rs.core.MultivaluedMap;

@Table
@Entity
public class MailingListSubscriptions extends BareNode {
    public static final DtoTable TABLE = new DtoTable(MailingListSubscriptions.class, "mls");

    @Id
    @Nonnull
    private String email;
    @Nonnull
    private String listName;
    private boolean digest;
    @Nonnull
    private Date subscribed;

    @JsonIgnore
    @Override
    public Object getId() {
        return getEmail();
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the listName
     */
    public String getListName() {
        return listName;
    }

    /**
     * @param listName the listName to set
     */
    public void setListName(String listName) {
        this.listName = listName;
    }

    /**
     * @return the digest
     */
    public boolean isDigest() {
        return digest;
    }

    /**
     * @param digest the digest to set
     */
    public void setDigest(boolean digest) {
        this.digest = digest;
    }

    /**
     * @return the subscribed
     */
    public Date getSubscribed() {
        return subscribed;
    }

    /**
     * @param subscribed the subscribed to set
     */
    public void setSubscribed(Date subscribed) {
        this.subscribed = subscribed;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(email, listName, digest, subscribed);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        MailingListSubscriptions other = (MailingListSubscriptions) obj;
        return Objects.equals(email, other.email) && Objects.equals(listName, other.listName)
                && Objects.equals(digest, other.digest) && Objects.equals(subscribed, other.subscribed);
    }

    @Singleton
    public static class MailingListSubscriptionsFilter implements DtoFilter<MailingListSubscriptions> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement stmt = builder.build(TABLE);
            if (isRoot) {
                // ID check
                String id = params.getFirst(DefaultUrlParameterNames.ID.getName());
                if (StringUtils.isNotBlank(id)) {
                    stmt.addClause(
                            new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".email = ?", new Object[] { id }));
                }
                // project ID check
                String listName = params.getFirst(MailingListUrlParameterNames.LIST_NAME.getName());
                if (StringUtils.isNotBlank(listName)) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".listName = ?",
                            new Object[] { listName }));
                }
            }
            return stmt;
        }

        @Override
        public Class<MailingListSubscriptions> getType() {
            return MailingListSubscriptions.class;
        }
    }
}
