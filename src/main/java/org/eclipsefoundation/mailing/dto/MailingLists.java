/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.mailing.dto;

import java.util.Date;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.mailing.namespace.MailingListUrlParameterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import jakarta.annotation.Nonnull;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.PostLoad;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.ws.rs.core.MultivaluedMap;

@Table
@Entity
public class MailingLists extends BareNode {
    public static final DtoTable TABLE = new DtoTable(MailingLists.class, "ml");

    @Id
    @Nonnull
    private String listName;
    private String listDescription;
    @Nonnull
    private String projectId;
    private String listShortDescription;
    @Nonnull
    private boolean createArchives;
    @Nonnull
    private boolean isForNewsArchives;
    @Nonnull
    private boolean isDisabled;
    @Nonnull
    private boolean isDeleted;
    @Nonnull
    private boolean isPrivate;
    @Nonnull
    private boolean isSubscribable;
    private Date createDate;
    private String createdBy;
    private String provisionStatus;
    @JsonInclude(value = Include.NON_NULL)
    @Transient
    private Long count;
    @Transient
    private String url;
    @Transient
    private String email;

    @PostLoad
    public void loadAdditionalProperties() {
        this.setUrl("https://accounts.eclipse.org/mailing-list/" + listName);
        this.setEmail(listName + "@eclipse.org");
    }

    @JsonIgnore
    @Override
    public Object getId() {
        return getListName();
    }

    /**
     * @return the listName
     */
    public String getListName() {
        return listName;
    }

    /**
     * @param listName the listName to set
     */
    public void setListName(String listName) {
        this.listName = listName;
    }

    /**
     * @return the listDescription
     */
    public String getListDescription() {
        return listDescription;
    }

    /**
     * @param listDescription the listDescription to set
     */
    public void setListDescription(String listDescription) {
        this.listDescription = listDescription;
    }

    /**
     * @return the projectId
     */
    public String getProjectId() {
        return projectId;
    }

    /**
     * @param projectId the projectId to set
     */
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    /**
     * @return the listShortDescription
     */
    public String getListShortDescription() {
        return listShortDescription;
    }

    /**
     * @param listShortDescription the listShortDescription to set
     */
    public void setListShortDescription(String listShortDescription) {
        this.listShortDescription = listShortDescription;
    }

    /**
     * @return the createArchives
     */
    public boolean isCreateArchives() {
        return createArchives;
    }

    /**
     * @param createArchives the createArchives to set
     */
    public void setCreateArchives(boolean createArchives) {
        this.createArchives = createArchives;
    }

    /**
     * @return the isForNewsArchives
     */
    @JsonProperty(value = "is_for_news_archives")
    public boolean isForNewsArchives() {
        return isForNewsArchives;
    }

    /**
     * @param isForNewsArchives the isForNewsArchives to set
     */
    public void setForNewsArchives(boolean isForNewsArchives) {
        this.isForNewsArchives = isForNewsArchives;
    }

    /**
     * @return the isDisabled
     */
    @JsonProperty(value = "is_disabled")
    public boolean isDisabled() {
        return isDisabled;
    }

    /**
     * @param isDisabled the isDisabled to set
     */
    public void setDisabled(boolean isDisabled) {
        this.isDisabled = isDisabled;
    }

    /**
     * @return the isDeleted
     */
    @JsonProperty(value = "is_deleted")
    public boolean isDeleted() {
        return isDeleted;
    }

    /**
     * @param isDeleted the isDeleted to set
     */
    public void setDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * @return the isPrivate
     */
    @JsonProperty(value = "is_private")
    public boolean isPrivate() {
        return isPrivate;
    }

    /**
     * @param isPrivate the isPrivate to set
     */
    public void setPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    /**
     * @return the isSubscribable
     */
    @JsonProperty(value = "is_subscribable")
    public boolean isSubscribable() {
        return isSubscribable;
    }

    /**
     * @param isSubscribable the isSubscribable to set
     */
    public void setSubscribable(boolean isSubscribable) {
        this.isSubscribable = isSubscribable;
    }

    /**
     * @return the createDate
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the provisionStatus
     */
    public String getProvisionStatus() {
        return provisionStatus;
    }

    /**
     * @param provisionStatus the provisionStatus to set
     */
    public void setProvisionStatus(String provisionStatus) {
        this.provisionStatus = provisionStatus;
    }

    /**
     * @return the count
     */
    public Long getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(Long count) {
        this.count = count;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result
                + Objects.hash(listName, listDescription, projectId, listShortDescription, createArchives,
                        isForNewsArchives, isDisabled, isDeleted, isPrivate, isSubscribable, createDate, createdBy,
                        provisionStatus, count, url, email);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        MailingLists other = (MailingLists) obj;
        return Objects.equals(listName, other.listName) && Objects.equals(listDescription, other.listDescription)
                && Objects.equals(projectId, other.projectId)
                && Objects.equals(listShortDescription, other.listShortDescription)
                && Objects.equals(createArchives, other.createArchives)
                && Objects.equals(isForNewsArchives, other.isForNewsArchives)
                && Objects.equals(isDisabled, other.isDisabled) && Objects.equals(isDeleted, other.isDeleted)
                && Objects.equals(isPrivate, other.isPrivate) && Objects.equals(isSubscribable, other.isSubscribable)
                && Objects.equals(createDate, other.createDate) && Objects.equals(createdBy, other.createdBy)
                && Objects.equals(provisionStatus, other.provisionStatus) && Objects.equals(count, other.count)
                && Objects.equals(url, other.url) && Objects.equals(email, other.email);
    }

    @Singleton
    public static class MailingListFilter implements DtoFilter<MailingLists> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement stmt = builder.build(TABLE);
            if (isRoot) {
                // ID check
                String id = params.getFirst(MailingListUrlParameterNames.LIST_NAME.getName());
                if (StringUtils.isNotBlank(id)) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".listName = ?",
                            new Object[] { id }));
                }
                // project ID check
                String projectId = params.getFirst(MailingListUrlParameterNames.PROJECT_ID.getName());
                if (StringUtils.isNotBlank(projectId)) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".projectId = ?",
                            new Object[] { projectId }));
                }
                // subscribable check
                String isSubscribable = params.getFirst(MailingListUrlParameterNames.IS_SUBSCRIBABLE.getName());
                if (StringUtils.isNotBlank(isSubscribable)) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".isSubscribable = ?",
                            new Object[] { Boolean.parseBoolean(isSubscribable) }));
                }
                // is private check
                String isPrivate = params.getFirst(MailingListUrlParameterNames.IS_PRIVATE.getName());
                if (StringUtils.isNotBlank(isPrivate)) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".isPrivate = ?",
                            new Object[] { Boolean.parseBoolean(isPrivate) }));
                }
                // is deleted check
                String isDeleted = params.getFirst(MailingListUrlParameterNames.IS_DELETED.getName());
                if (StringUtils.isNotBlank(isDeleted)) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".isDeleted = ?",
                            new Object[] { Boolean.parseBoolean(isDeleted) }));
                }
                // is disabled check
                String isDisabled = params.getFirst(MailingListUrlParameterNames.IS_DISABLED.getName());
                if (StringUtils.isNotBlank(isDisabled)) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".isDisabled = ?",
                            new Object[] { Boolean.parseBoolean(isDisabled) }));
                }
                // subscriber check
                String userEmail = params.getFirst(MailingListUrlParameterNames.USER_EMAIL.getName());
                if (StringUtils.isNotBlank(userEmail)) {
                    stmt.addJoin(new ParameterizedSQLStatement.Join(TABLE, MailingListSubscriptions.TABLE, "listName",
                            "listName"));
                    stmt.addClause(new ParameterizedSQLStatement.Clause(
                            MailingListSubscriptions.TABLE.getAlias() + ".email = ?", new Object[] { userEmail }));
                }
            }
            return stmt;
        }

        @Override
        public Class<MailingLists> getType() {
            return MailingLists.class;
        }
    }
}
