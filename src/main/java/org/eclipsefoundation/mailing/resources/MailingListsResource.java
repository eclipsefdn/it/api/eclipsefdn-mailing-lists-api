/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.mailing.resources;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.services.ProfileService;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.http.namespace.CacheControlCommonValues;
import org.eclipsefoundation.mailing.dto.MailingListSubscriptions;
import org.eclipsefoundation.mailing.dto.MailingLists;
import org.eclipsefoundation.mailing.namespace.MailingListUrlParameterNames;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.eclipsefoundation.utils.helper.TransformationHelper;
import org.jboss.resteasy.reactive.Cache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;

@Path("mailing-list")
@Produces(MediaType.APPLICATION_JSON)
public class MailingListsResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(MailingListsResource.class);

    private static final String RESULT_COUNT_MSG = "Found {} MailingList results";

    @Inject
    DefaultHibernateDao dao;
    @Inject
    FilterService filters;
    @Inject
    RequestWrapper wrap;

    @Inject
    ProfileService profileService;

    /**
     * Returns a Response containing all MailingLists entities. This list can be filtered by EF username.
     * Returns a 404 Not Found if the username was provided and the user cannot be found.
     * 
     * @param username The username filter for mailing lists
     * @return A Response containing all MailingLists filtered by username. A 404 Response when the user does not exist.
     */
    @GET
    @Cache(maxAge = CacheControlCommonValues.SAFE_CACHE_MAX_AGE)
    public Response all(@QueryParam("username") String username) {
        MultivaluedMap<String, String> params = getDefaultParams();
        // check for user to retrieve
        if (StringUtils.isNotBlank(username)) {
            Optional<EfUser> user = profileService.fetchUserByUsername(username, true);
            if (user.isEmpty()) {
                throw new NotFoundException(String.format("Could not find valid user: %s", username));
            }

            // Add email to params if it exists
            if (StringUtils.isNotBlank(user.get().mail())) {
                LOGGER.debug("Adding email '{}'' to query params", user.get().mail());
                params.add(MailingListUrlParameterNames.USER_EMAIL.getName(), user.get().mail());
            }
        }
        return Response.ok(dao.get(new RDBMSQuery<>(wrap, filters.get(MailingLists.class), params))).build();
    }

    /**
     * Returns a Response containing all MailingLists entities that match the given list name. 
     * Returns a 404 Not Found if no mailing lists can be found.
     * 
     * @param listName The desired list name
     * @return A Response containing all MailingLists matching the given name. A 404 Response when the lists cannot be found.
     */
    @GET
    @Path("{listName}")
    @Cache(maxAge = CacheControlCommonValues.AGGRESSIVE_CACHE_MAX_AGE)
    public Response mailingList(@PathParam("listName") String listName) {
        MultivaluedMap<String, String> params = getDefaultParams();
        params.add(MailingListUrlParameterNames.LIST_NAME.getName(), listName);
        List<MailingLists> mailingLists = dao.get(new RDBMSQuery<>(wrap, filters.get(MailingLists.class), params));
        LOGGER.debug(RESULT_COUNT_MSG, mailingLists.size());
        if (mailingLists.isEmpty()) {
            throw new NotFoundException(
                    String.format("Mailing-list '%s' was not found", TransformationHelper.formatLog(listName)));
        }
        // add count to singular mailing list retrieval
        MailingLists ml = mailingLists.get(0);
        ml.setCount(dao.count(new RDBMSQuery<>(wrap, filters.get(MailingListSubscriptions.class), params)));
        return Response.ok(Arrays.asList(ml)).build();
    }

    /**
     * Returns a Response containing all Available MailingLists entities. Lists are considered 'available' when 
     * they are neither deleted nor disabled, and are subscribable.
     * 
     * @return A Response containing all available MailingLists matching the given name.
     */
    @GET
    @Path("available")
    @Cache(maxAge = CacheControlCommonValues.AGGRESSIVE_CACHE_MAX_AGE)
    public Response available() {
        List<MailingLists> mailingLists = dao
                .get(new RDBMSQuery<>(wrap, filters.get(MailingLists.class), getAvailableListParams()));
        LOGGER.debug(RESULT_COUNT_MSG, mailingLists.size());
        return Response.ok(mailingLists).build();
    }

    /**
     * Returns a Response containing all MailingLists entities grouped by project name. 
     * Returns a 200 OK with an emtpy array if no mailing lists can be found.
     * 
     * @return A Response containing all MailingLists grouped by project if available.
     */
    @GET
    @Path("projects")
    @Cache(maxAge = CacheControlCommonValues.AGGRESSIVE_CACHE_MAX_AGE)
    public Response getByProjects() {
        List<MailingLists> mailingLists = dao
                .get(new RDBMSQuery<>(wrap, filters.get(MailingLists.class), getDefaultParams()));
        LOGGER.debug(RESULT_COUNT_MSG, mailingLists.size());
        if (mailingLists.isEmpty()) {
            return Response.ok(Collections.emptyList()).build();
        }

        return Response.ok(mailingLists.stream().collect(Collectors.groupingBy(MailingLists::getProjectId))).build();
    }

    /**
     * Returns a Response containing all MailingLists entities that match the given project id. 
     * Returns a 200 OK with an emtpy array if no mailing lists can be found.
     * 
     * @param projectId The given project id
     * @return  A Response containing all MailingLists for the given project if available.
     */
    @GET
    @Path("projects/{projectId}")
    @Cache(maxAge = CacheControlCommonValues.AGGRESSIVE_CACHE_MAX_AGE)
    public Response getForProjectByID(@PathParam("projectId") String projectId) {
        MultivaluedMap<String, String> params = getDefaultParams();
        params.add(MailingListUrlParameterNames.PROJECT_ID.getName(), projectId);
        List<MailingLists> mailingLists = dao.get(new RDBMSQuery<>(wrap, filters.get(MailingLists.class), params));
        LOGGER.debug(RESULT_COUNT_MSG, mailingLists.size());
        if (mailingLists.isEmpty()) {
            return Response.ok(Collections.emptyList()).build();
        }
        return Response.ok(mailingLists).build();
    }

    /**
     * Returns a Response containing all Available MailingLists entities, grouped by project name. 
     * Lists are considered 'available' when they are neither deleted nor disabled, and are subscribable.
     * 
     * @return A Response containing all available MailingLists matching the given name.
     */
    @GET
    @Path("projects/available")
    @Cache(maxAge = CacheControlCommonValues.AGGRESSIVE_CACHE_MAX_AGE)
    public Response getByProjectsAvailable() {
        List<MailingLists> mailingLists = dao
                .get(new RDBMSQuery<>(wrap, filters.get(MailingLists.class), getAvailableListParams()));
        LOGGER.debug(RESULT_COUNT_MSG, mailingLists.size());
        if (mailingLists.isEmpty()) {
            return Response.ok(Collections.emptyList()).build();
        }

        return Response.ok(mailingLists.stream().collect(Collectors.groupingBy(MailingLists::getProjectId))).build();
    }

    /**
     * Returns a reference to a MultivaluedMap containing the default mailing list search params.
     * 
     * @return A param map with 'is_private' mapped to 'false'
     */
    private MultivaluedMap<String, String> getDefaultParams() {
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(MailingListUrlParameterNames.IS_PRIVATE.getName(), Boolean.FALSE.toString());
        return params;
    }

    /**
     * Returns a reference to a MultivaluedMap containing the 'Available' mailing list search params.
     * 
     * @return A param map with 'is_deleted' and 'is_disabled' mapped to false, and 'is_subscribable' mapped to 'true'
     */
    private MultivaluedMap<String, String> getAvailableListParams() {
        MultivaluedMap<String, String> params = getDefaultParams();
        params.add(MailingListUrlParameterNames.IS_DELETED.getName(), Boolean.FALSE.toString());
        params.add(MailingListUrlParameterNames.IS_DISABLED.getName(), Boolean.FALSE.toString());
        params.add(MailingListUrlParameterNames.IS_SUBSCRIBABLE.getName(), Boolean.TRUE.toString());
        return params;
    }
}