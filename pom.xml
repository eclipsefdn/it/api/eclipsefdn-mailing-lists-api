<?xml version="1.0"?>
<project
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd"
  xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <modelVersion>4.0.0</modelVersion>
  <groupId>org.eclipsefoundation</groupId>
  <artifactId>eclipsefdn-mailing-lists</artifactId>
  <version>1.0.0-SNAPSHOT</version>

  <properties>
    <eclipse-api-version>1.2.1</eclipse-api-version>
    <compiler-plugin.version>3.13.0</compiler-plugin.version>
    <maven.compiler.source>17</maven.compiler.source>
    <maven.compiler.target>17</maven.compiler.target>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
    <quarkus.platform.artifact-id>quarkus-bom</quarkus.platform.artifact-id>
    <quarkus.platform.group-id>io.quarkus.platform</quarkus.platform.group-id>
    <quarkus.platform.version>3.15.3.1</quarkus.platform.version>
    <surefire-plugin.version>3.3.1</surefire-plugin.version>
    <maven.compiler.parameters>true</maven.compiler.parameters>
    <org.mapstruct.version>1.5.5.Final</org.mapstruct.version>
    <sonar.sources>src/main</sonar.sources>
    <sonar.tests>src/test</sonar.tests>
    <sonar.java.coveragePlugin>jacoco</sonar.java.coveragePlugin>
    <sonar.dynamicAnalysis>reuseReports</sonar.dynamicAnalysis>
    <sonar.coverage.jacoco.xmlReportPaths>${project.basedir}/target/jacoco-report/jacoco.xml</sonar.coverage.jacoco.xmlReportPaths>
    <sonar.junit.reportPath>${project.build.directory}/surefire-reports</sonar.junit.reportPath>
    <sonar.host.url>https://sonarcloud.io</sonar.host.url>
    <sonar.organization>eclipse-foundation-it</sonar.organization>
    <sonar.projectKey>eclipsefdn-mailing-lists-api</sonar.projectKey>
    <sonar.projectName>Mailing-Lists API</sonar.projectName>
  </properties>

  <repositories>
    <repository>
      <id>eclipsefdn</id>
      <url>https://repo.eclipse.org/content/repositories/eclipsefdn/</url>
      <releases>
        <enabled>true</enabled>
      </releases>
      <snapshots>
        <enabled>true</enabled>
      </snapshots>
    </repository>
  </repositories>

  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>${quarkus.platform.group-id}</groupId>
        <artifactId>${quarkus.platform.artifact-id}</artifactId>
        <version>${quarkus.platform.version}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
    </dependencies>
  </dependencyManagement>

  <dependencies>
    <dependency>
      <groupId>org.eclipsefoundation</groupId>
      <artifactId>quarkus-core</artifactId>
      <version>${eclipse-api-version}</version>
      <!-- Can be removed once dependency is removed from base package
      https://stackoverflow.com/questions/67510802/logging-in-quarkus-works-in-dev-mode-but-doesnt-output-in-jvm-docker-image -->
      <exclusions>
        <exclusion>
          <groupId>org.jboss.logmanager</groupId>
          <artifactId>jboss-logmanager</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.eclipsefoundation</groupId>
      <artifactId>quarkus-persistence</artifactId>
      <version>${eclipse-api-version}</version>
    </dependency>
    <dependency>
      <groupId>org.eclipsefoundation</groupId>
      <artifactId>quarkus-efservices</artifactId>
      <version>${eclipse-api-version}</version>
    </dependency>

    <!-- Required for Hibernate naming strategy > Look for smaller alt package for Google core -->
    <dependency>
      <groupId>com.google.guava</groupId>
      <artifactId>guava</artifactId>
    </dependency>

    <!-- Testing dependencies only -->
    <dependency>
      <groupId>org.eclipsefoundation</groupId>
      <artifactId>quarkus-test-common</artifactId>
      <version>${eclipse-api-version}</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>io.quarkus</groupId>
      <artifactId>quarkus-jacoco</artifactId>
      <scope>test</scope>
    </dependency>

    <!-- Following H2/devservices deps are made to circumvent need for docker -->
    <dependency>
      <groupId>io.quarkus</groupId>
      <artifactId>quarkus-devservices-h2</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>io.quarkus</groupId>
      <artifactId>quarkus-jdbc-h2</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>com.h2database</groupId>
      <artifactId>h2</artifactId>
      <scope>test</scope>
    </dependency>
    <!-- Flyway specific dependencies, used to setup tables in test -->
    <dependency>
      <groupId>io.quarkus</groupId>
      <artifactId>quarkus-flyway</artifactId>
      <scope>test</scope>
    </dependency>
  </dependencies>

  <build>
    <plugins>
      <plugin>
        <groupId>${quarkus.platform.group-id}</groupId>
        <artifactId>quarkus-maven-plugin</artifactId>
        <version>${quarkus.platform.version}</version>
        <extensions>true</extensions>
        <executions>
          <execution>
            <goals>
              <goal>build</goal>
              <goal>generate-code</goal>
              <goal>generate-code-tests</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>${surefire-plugin.version}</version>
        <configuration>
          <skipTests>false</skipTests>
          <systemPropertyVariables>
            <java.util.logging.manager>org.jboss.logmanager.LogManager</java.util.logging.manager>
            <maven.home>${maven.home}</maven.home>
            <maven.repo>${settings.localRepository}</maven.repo>
          </systemPropertyVariables>
        </configuration>
      </plugin>
    </plugins>
  </build>

  <profiles>
    <profile>
      <id>native</id>
      <activation>
        <property>
          <name>native</name>
        </property>
      </activation>
      <build>
        <plugins>
          <plugin>
            <artifactId>maven-failsafe-plugin</artifactId>
            <version>${surefire-plugin.version}</version>
            <executions>
              <execution>
                <goals>
                  <goal>integration-test</goal>
                  <goal>verify</goal>
                </goals>
                <configuration>
                  <systemPropertyVariables>
                    <native.image.path>
                      ${project.build.directory}/${project.build.finalName}-runner</native.image.path>
                    <java.util.logging.manager>org.jboss.logmanager.LogManager</java.util.logging.manager>
                    <maven.home>${maven.home}</maven.home>
                  </systemPropertyVariables>
                </configuration>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
      <properties>
        <quarkus.package.type>native</quarkus.package.type>
      </properties>
    </profile>
  </profiles>
</project>