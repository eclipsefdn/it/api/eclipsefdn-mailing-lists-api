# eclipsefdn-mailing-lists-api

Eclipse Foundation API service for the retrieval of active mailing list data for projects within our ecosystem.

## Starting a development environment

### Requirements

* Docker
* Java 11 >  
* mvn
* node.js + npm  
* make
* Running MariaDB instance

### Setup

Before running the application, some setup is required.  

1. Run `make pre-setup`. This command will setup a basic .env file, using some basic variables used in Eclipse development to accelerate the generation of secret files.
2. Ensure that the `.env` file properties reflect the connection settings used for the MariaDB instance and the credentials needed to access the DB created in step 1.
3. Run `make setup` to finish the setup process for this API, generating the secret file under `./config/application/secret.properties`.
4. To finish the setup, open the secrets file and add values to the missing fields before starting the server.

#### Build and Start Server

```bash
make compile-start
```

#### Live-coding Dev Mode

```bash
make dev-start
```

#### Generate Spec

```bash
make compile-test-resources
```

#### Running Tests

```bash
mvn test
```

#### Render a Live UI Preview of the API Spec

```bash
make start-spec
```

## Contributing

1. [Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) the [eclipsefdn-mailing-lists-api](https://gitlab.eclipse.org//eclipsefdn/it/api/eclipsefdn-mailing-lists-api) repository
2. Clone repository `git clone https://gitlab.eclipse.org/[your_eclipsefdn_username]/eclipsefdn-mailing-lists-api.git`
3. Create your feature branch: `git checkout -b my-new-feature`
4. Commit your changes: `git commit -m 'Add some feature' -s`
5. Push feature branch: `git push origin my-new-feature`
6. Submit a merge request

### Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
[http://www.eclipse.org/legal/epl-2.0].

SPDX-License-Identifier: EPL-2.0
